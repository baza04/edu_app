import React from "react";
import s from './container.module.scss'
import HeaderBlock from "../HeaderBlock";
import ContentBlock from "../ContentBlock";
import FooterBlock from "../FooterBlock";
import Card from "../Card"

const ContainerBlock = ({arr}) => {

    const wordsList = [
        {
            eng: 'between',
            rus: 'между'
        },
        {
            eng: 'high',
            rus: 'высокий'
        },
        {
            eng: 'really',
            rus: 'действительно'
        },
        {
            eng: 'something',
            rus: 'что-нибудь'
        },
        {
            eng: 'most',
            rus: 'большинство'
        },
        {
            eng: 'another',
            rus: 'другой'
        },
        {
            eng: 'much',
            rus: 'много'
        },
        {
            eng: 'family',
            rus: 'семья'
        },
        {
            eng: 'own',
            rus: 'личный'
        },
        {
            eng: 'out',
            rus: 'из/вне'
        },
        {
            eng: 'leave',
            rus: 'покидать'
        },
    ];
    const content = [{
            url: 'https://miro.medium.com/max/3000/1*_rFlOSxgyg5ck1JqHoCJDA.jpeg',
            label: "ability",
            desc: "the physical or mental power or skill needed to do something: There's no doubting her ability.",
            ex: ["There's no doubting her ability.",
                "[ + to infinitive ] She had the ability to explain things clearly and concisely.",
                "She's a woman of considerable abilities.",
                "I have children in my class of very mixed abilities (= different levels of skill or intelligence).",
                "a mixed-ability class"]
        },
        {
            url: 'https://avatars.mds.yandex.net/get-zen_doc/1909279/pub_5ce31fb10f23c200b3a4967a_5ce3bdbdda1a5501ae6cf70b/scale_1200',
            label: "skill",
            desc: "an ability to do an activity or job well, especially because you have practised it:",
            ex: ["Ruth had/possessed great writing skills.",
                "I have no skill at/in sewing."]
        }]

    const listEx = (ex) => ex.map((number) => <li>{number}</li>)

    return (
        <div className={s.container} /*style={styleCover}*/>

            <HeaderBlock
                title='Learn The Words'
                desc='Learn The Words - it is your little helper for learning new words'
            />

            <ContentBlock
                url={content[0].url}
                label={content[0].label}
                desc={content[0].desc}
                ex={listEx(content[0].ex)}
            />

            <div className="Cont">
                <div className="cardCont">
                    {wordsList.map(({eng, rus}, index) => <Card key={index} eng={eng} rus={rus}/>)}
                </div>
            </div>


            <FooterBlock
                imgUrl= 'https://pngimg.com/uploads/github/github_PNG63.png'
                title = 'baza04'
                url = 'https://github.com/baza04'
            />
        </div>
    )
}

export default ContainerBlock
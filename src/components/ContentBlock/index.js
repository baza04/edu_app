import React from "react";
import s from './content.module.scss'

const ContentBlock = ({label, desc, ex, url}) => {
    return (
        <div className={s.ContentBlock}>
            {url && <div className={s.img}><img src={url} alt=""/></div>}
            <div className={s.text}>
                {label && <h2>{label}</h2>}
                {desc && <p>{desc}</p>}
                {ex && <ul><li>{ex}</li></ul>}
            </div>
        </div>
    )
}

export default ContentBlock
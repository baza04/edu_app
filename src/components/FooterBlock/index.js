import React from "react";
import s from './footer.module.scss'


const FooterBlock = ({imgUrl, title, url}) => {
    return (
            <div className={s.footer}>
                {title && url && <a href={url}> <img className={s.img} src={imgUrl} alt="" /> {title}</a>}
            </div>
    )
}

export default FooterBlock